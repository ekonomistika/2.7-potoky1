import java.time.LocalTime;
import java.util.*;

public class CoordinatedCheckIn {

    public static final Object Market = new Object();

    public static enum Company {
        AAPL,
        COKE,
        IBM
    }

    ;

    public static Share[] sharesMarket  = {new Share(Company.AAPL, 100, 141), new Share(Company.COKE, 1000, 378), new Share(Company.IBM, 200, 137)};
    public static Share[] aliceShars    = {new Share(Company.AAPL, 10, 100), new Share(Company.COKE, 20, 390)};
    public static Share[] bobShars      = {new Share(Company.AAPL, 10, 140), new Share(Company.IBM, 20, 135)};
    public static Share[] charliShars   = {new Share(Company.COKE, 300, 370)};

    public static void coordinatedCheckIn() {

        if (Thread.currentThread().getName().equals("Alice")) {
        for (Share shares : sharesMarket) {
                for (Share sharesAlice : aliceShars) {
                    if (sharesAlice.shar.equals(shares.shar)) {
                        if (shares.price < sharesAlice.price) {
                            System.out.println("Покупаем " + shares.shar + " в количестве " + Math.min(shares.amount, sharesAlice.amount) + " покупатель " + "Alice");
                        } else {
                            System.out.println("НЕ Покупаем " + shares.shar + " в количестве " + Math.min(shares.amount, sharesAlice.amount) + " покупатель " + "Alice");
                        }
                        System.out.println(shares.shar);
                    }
                }
            }
                try {
                    synchronized (Market) {
                        Market.wait(5000);
                        System.out.println(LocalTime.now() + ": " + Thread.currentThread().getName() + " зареєстровано");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(Thread.currentThread().getName().equals("Bob")) {
                for (Share shares : sharesMarket) {
                    for (Share sharesBob : bobShars) {
                        if (sharesBob.shar.equals(shares.shar)) {
                            if (shares.price < sharesBob.price) {
                                System.out.println("Покупаем " + shares.shar + " в количестве " + Math.min(shares.amount, sharesBob.amount) + " покупатель " + "Bob");
                            } else {
                                System.out.println("НЕ Покупаем " + shares.shar + " в количестве " + Math.min(shares.amount, sharesBob.amount) + " покупатель " + "Bob");
                            }
                            System.out.println(shares.shar);
                        }
                    }
                    try {
                        synchronized (Market) {
                            Market.wait(5000);
                            System.out.println(LocalTime.now() + ": " + Thread.currentThread().getName() + " зареєстровано");
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            if(Thread.currentThread().getName().equals("Charlie")) {
                for (Share shares : sharesMarket) {
                    for (Share sharesCharlie : charliShars) {
                        if (sharesCharlie.shar.equals(shares.shar)) {
                            if (shares.price < sharesCharlie.price) {
                                System.out.println("Покупаем " + shares.shar + " в количестве " + Math.min(shares.amount, sharesCharlie.amount) + " покупатель " + "Charlie");
                            } else {
                                System.out.println("НЕ Покупаем " + shares.shar + " в количестве " + Math.min(shares.amount, sharesCharlie.amount) + " покупатель " + "Charlie");
                            }
                            System.out.println(shares.shar);
                        }
                    }
                    try {
                        synchronized (Market) {
                            Market.wait(5000);
                            System.out.println(LocalTime.now() + ": " + Thread.currentThread().getName() + " зареєстровано");
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    public static void main (String[]args){

                System.out.println(LocalTime.now() + ": Початок виконання");

                new Thread(CoordinatedCheckIn::coordinatedCheckIn, "Alice").start();
                new Thread(CoordinatedCheckIn::coordinatedCheckIn, "Bob").start();
                new Thread(CoordinatedCheckIn::coordinatedCheckIn, "Charlie").start();


                for (int i = 0; i < 3; i++) {

                    for (Share marketShars: sharesMarket ){
                        int i0 = (int) (Math.random() * 3);
                        marketShars.price = marketShars.price *(1+i0/100);
                    }

                    try {
                        Thread.sleep(30000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (Market) {
                        Market.notify();
                    }
                }
            }
            static class Share {
                public Company shar;
                public Integer amount;
                public Integer price;

                Share(Company shar, Integer amount, Integer price) {
                    this.amount = amount;
                    this.price = price;
                    this.shar = shar;
                }

            }
}

